#!/bin/bash
# BEGIN 99-ethtool.sh
# This file must be made executable!
#
# Use case: NetworkManager does not support every possible ethtool command.
#
# If an ifcfg file has ETHTOOL_CMD set then the string is split on semicolon (;)
# and each substring is passed to ethtool.
#
# ex: ETHTOOL_CMD="-G ${DEVICE} rx 4096; -K ${DEVICE} gso off"
#

if [[ $2 == up ]]; then
  SCRIPT="$(basename "$0")"
  if [[ -e $CONNECTION_FILENAME ]]; then
    source $CONNECTION_FILENAME
    if [[ -n $ETHTOOL_CMD ]]; then
      readarray -d \; -t cmd_array <<< "$ETHTOOL_CMD"
      for cmd in "${cmd_array[@]}"; do
        if [[ -z $cmd ]]; then
          continue
        fi
        ethtoolcmd="/usr/sbin/ethtool $cmd"
        if $ethtoolcmd; then 
          logger "$SCRIPT: success for $1: $ethtoolcmd"
        else
          logger "$SCRIPT: failed for $1: $ethtoolcmd"
        fi
      done
    fi
  else
    logger "$SCRIPT: $CONNECTION_FILENAME does not exist for $1?"
  fi
fi

# END 99-ethtool.sh
