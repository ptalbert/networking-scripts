#!/bin/bash
# BEGIN 99-virsh-reconnect.sh
# This file must be made executable!
#
# Use case: libvirt does not recognize when a shared device (bridge) is removed
# and readded. If this happens while a guest that is using the bridge is running
# the tap device is not reconnected.
# Whenever a bridge interface is activated by NM this dispatcher script will
# look for running domains that use the bridge and attempt to reconnect them.

INTERFACE=$1
ACTION=$2

if [[ $ACTION == up && -d /sys/class/net/$INTERFACE/bridge ]]; then
  SCRIPT="$(basename "$0")"
  for domain in $(virsh list --name 2>/dev/null); do
    if [[ -z $domain ]]; then
      continue
    fi

    for vnet in $(virsh -q domiflist $domain 2>/dev/null | awk -v brname=$INTERFACE '($2 == "bridge" && $3 == brname) {print $1}'); do
      ipcmd="ip link set dev $vnet master $INTERFACE"
      if $ipcmd; then
        logger "$SCRIPT: success for domain $domain: $ipcmd"
      else
        logger "$SCRIPT: failed for domain $domain: $ipcmd"
      fi
    done
  done
fi

# END 99-virsh-reconnect.sh
