#!/bin/bash
# monitor2.sh begins here
# ptalbert@redhat.com

VERSION=0.9.5
SCRIPT="$(basename "$0")"
HELPREGEX="^[-/]*h"
PERIOD=30
ITERATIONS=0
DATEFORMAT="+%c %s.%N"
CMDTIMEOUT=0

doHelp() {
  echo " Usage: $SCRIPT [<PERIOD>] [<ITERATIONS>]"
  echo
  echo "    <PERIOD> is the seconds between recordings"
  echo "         The default period is $PERIOD seconds"
  echo
  echo "    <ITERATIONS> is the number of recordings to take"
  echo "         The default is $ITERATIONS iterations"
  echo "         (0 is unlimited)"
}

# array of commands to run once at startup
command_once_array+=("uname -a")
command_once_array+=("sysctl -a")
command_once_array+=("ip -d address")
command_once_array+=("ethtool -g %eth%")
command_once_array+=("ethtool -i %eth%")
command_once_array+=("ethtool -k %eth%")

# add iptables commands if the modules are loaded...
if lsmod | grep --quiet ip_tables &>/dev/null
then
  command_once_array+=("iptables -nvxL -t filter")
  command_once_array+=("iptables -nvxL -t nat")
  command_once_array+=("iptables -nvxL -t mangle")
  command_once_array+=("iptables -nvxL -t raw")
fi

# array of files to record once at startup
file_once_array+=("/proc/sys/kernel/tainted")

# array of commands to run on every iteration
command_iter_array+=("ip -s -s neigh show")
command_iter_array+=("tc -s qdisc")
command_iter_array+=("nstat -az")
command_iter_array+=("ss -noempitau")
#command_iter_array+=("ss -noemitau")
command_iter_array+=("ps -alfe")
command_iter_array+=("mpstat -A")
command_iter_array+=("top -c -b -n1")
command_iter_array+=("numastat")
command_iter_array+=("ethtool -S %eth%")
command_iter_array+=("ip -s -s -d link")

# array of files to record on every iteration
file_iter_array+=("/proc/interrupts")
file_iter_array+=("/proc/softirqs")
file_iter_array+=("/proc/vmstat")
file_iter_array+=("/proc/net/softnet_stat")
file_iter_array+=("/proc/net/sockstat")
file_iter_array+=("/proc/net/sockstat6")
file_iter_array+=("/sys/class/net/%eth%/statistics/*")
file_iter_array+=("/proc/net/bonding/*")

doLog() {
  logtype=$1
  logitem=$2
  logfile="$OUTPUTPATH/$3"
  echo "===== $(date "$DATEFORMAT") =====" >> "$logfile"
  case "$logtype" in
    FILE)
      grep "" $logitem >> "$logfile" 2>&1 ;;
    EXEC)
      $logitem >> "$logfile" 2>&1 ;;
    ERROR)
      echo "$logitem" >> "$logfile" ;;
  esac
}

makeFilename() {
  filename="${1//[ \/\*]/_}"
  filename="${filename#_}"
  filename="${filename%_*_}"
}

getInterfaces() {
  for netdev in /sys/class/net/*; do
    netdev=$(basename "$netdev")
    if [[ $netdev != "lo" && $netdev != "bonding_masters" ]]; then
      interfaceArray+=("$netdev")
    fi
  done
}

filterArray() {
  unset -v filteredArray
  declare -ag 'filteredArray=("${'"$1"'[@]}")'
  for index in "${!filteredArray[@]}"; do
    item="${filteredArray[$index]}"
    makeFilename "$item"
    program=${item%% *}

    if [[ $1 == "command"* ]] && ! command -v "$program" &>/dev/null; then
      logitem=" info: Command not found in the path, skipping: $program" && echo "$logitem"
      doLog ERROR "$logitem" "$filename"
      unset -v "filteredArray[$index]"
      continue
    fi

    if [[ $item == *"%eth%"* ]]; then
      for netdev in "${interfaceArray[@]}"; do
        new_item="${item//%eth%/$netdev}"
        filteredArray+=("$new_item")
      done
      unset -v "filteredArray[$index]"
    fi
  done
}

runPrograms() {
  local -a 'rparray=("${'"$1"'[@]}")'
  for item in "${rparray[@]}"; do
    makeFilename "$item"
    case "$1" in
      command*)
        logtype=EXEC
        if [[ $CMDTIMEOUT -gt 0 ]]; then
          input="timeout $CMDTIMEOUT $item"
        else
          input="$item"
        fi
        ;;
      file*)
        logtype=FILE
        input="$item"
        ;;
    esac
    doLog $logtype "$input" "$filename"
  done
}

if [[ $# -gt 2 || $1 =~ $HELPREGEX ]]; then doHelp; exit 1; fi
if [[ -n $1 && "$1" -eq "$1" ]]; then PERIOD=$1; fi
if [[ -n $2 && "$2" -eq "$2" ]]; then ITERATIONS=$2; fi

counter=$ITERATIONS
while
  OUTPUTPATH="${SCRIPT%%.*}_${HOSTNAME%%.*}_$(date +%Y-%m-%d-%H)"
  if [[ ! -d "$OUTPUTPATH" || -z $firstrun ]]; then
    if ! mkdir -p "$OUTPUTPATH"; then doHelp; exit 1; fi
    echo && cat <<-EOF >> "$OUTPUTPATH"/monitor2.ver 
    $(date "$DATEFORMAT")
    $SCRIPT version $VERSION
     output path: $OUTPUTPATH
      iterations: $counter remain (0 = infinite)
          period: $PERIOD seconds
    EOF
    tail -n 5 "$OUTPUTPATH"/monitor2.ver && echo
    getInterfaces
    filterArray command_once_array && command_once_filtered=("${filteredArray[@]}")
    filterArray command_iter_array && command_iter_filtered=("${filteredArray[@]}")
    filterArray file_once_array && file_once_filtered=("${filteredArray[@]}")
    filterArray file_iter_array && file_iter_filtered=("${filteredArray[@]}")
    echo -en "\nRunning: "
    runPrograms command_once_filtered
    runPrograms file_once_filtered
    firstrun="done"
  fi
  runPrograms command_iter_filtered
  runPrograms file_iter_filtered
  echo -n "." ; ((counter--))
  if [[ $ITERATIONS -ne 0 && $counter -le 0 ]]; then break; fi
do
  sleep "$PERIOD"
done
echo
# monitor2.sh ends here
